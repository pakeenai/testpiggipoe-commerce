module.exports = {
  apps : [{
    name      : 'TEst',
    script    : 'api.js',
    log_date_format: "YYYY-MM-DD HH:mm:ss",
    merge_logs: true,
    node_args: "--max-http-header-size=65535",
    env: {
      NODE_ENV: 'development',
      LISTENPORT: 9014
    },
    env_production : {
      NODE_ENV: 'production',
      LISTENPORT: 9028
    },
    env_production2 : {
      NODE_ENV: 'production2',
      LISTENPORT: 9028
    },
    env_uat : {
      NODE_ENV: 'uat',
      LISTENPORT: 9028
    }
  }],
};
