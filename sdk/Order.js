const _ = require('lodash');
var qs = require('qs');
const config = require('./config/' + process.env.NODE_ENV);
const storage = require('node-sessionstorage')
var mysql = require('mysql2');
const d = new Date();
var time = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds()

class Order {

    async AddOrder(req) {
        const postData = req.body;
        let id = postData.id
        if (id == "" || id == undefined) {
            return {
                status: false,
                message: 'pls check id!'
            }
        }
        const sql_findorder = "SELECT * FROM test_purchaseorder as pur where pur.puord_status = 'process' and pur.user_id = " + "'" + id.toString() + "'"
        return new Promise((resolve, reject) => {
            try {
                global.TestDb.query(sql_findorder, function (err, rows, fields) {
                    if (err) {
                        console.log('err qeury:', err)
                        throw err
                    }
                    if (rows.length > 0) {
                        resolve({
                            status: false,
                            message: 'Sorry, Have Order is already in the system.'
                        })
                    } else {
                        try {
                            const sql = "select sum(pro.prod_price) as price from test_order as ord" +
                                    " LEFT JOIN test_products as pro ON ord.prod_id = pro.prod_id where ord.user_id = " + "'" + id.toString() + "'" + " and ord.ord_status = 'open'"
                            global.TestDb.query(sql, function (err, rows, fields) {
                                if (err) {
                                    console.log('err qeury:', err)
                                    throw err
                                }
                                var sql = "INSERT INTO test_purchaseorder (user_id,puord_status,puord_totalprice,create_date) VALUES ('" + id.toString() + "','process','" + rows[0].price + "','" + time + "')"
                                global.TestDb.query(sql, function (err, rows, fields) {
                                    if (err) {
                                        console.log('err qeury:', err)
                                        throw err
                                    }
                                })
                                resolve({
                                    status: true,
                                    message: 'Create Order Success!'
                                })
                            })
                        } catch (error) {
                            reject({
                                status: false,
                                message: 'Error db!'
                            })
                            console.error('error find user from db :', e)
                        }
                    }

                })
            } catch (e) {
                reject({
                    status: false,
                    message: 'Error db!'
                })
                console.error('error find user from db :', e)
            }
        });
    }
    async CancelOrder(req) {
        const postData = req.query;
        let id = postData.id
        if (id == "" || id == undefined) {
            return {
                status: false,
                message: 'pls check id!'
            }
        }
        const sql = "UPDATE test_purchaseorder SET puord_status = 'close' WHERE puord_id = " + "'" + id.toString() + "'"
        return new Promise((resolve, reject) => {
            try {
                global.TestDb.query(sql, function (err, rows, fields) {
                    if (err) {
                        console.log('err qeury:', err)
                        throw err
                    }
                    resolve({
                        status: true,
                        message: 'Update Success!'
                    })
                })
            } catch (e) {
                reject({
                    status: false,
                    message: 'Error db!'
                })
                console.error('error find user from db :', e)
            }
        });
    }
    async getOrder(req) {
        const postData = req.query;
        let id = postData.id
        if (id == "" || id == undefined) {
            return {
                status: false,
                message: 'pls check id!'
            }
        }
        return new Promise((resolve, reject) => {
            const sql = "SELECT purorder.puord_id,purorder.puord_status,purorder.puord_totalprice,JSON_ARRAYAGG(JSON_OBJECT('name',pro.prod_name ,'price',ord.ord_price,'status',ord.ord_status)) as products FROM test_purchaseorder as purorder LEFT JOIN test_order as ord ON ord.user_id = purorder.user_id " +
                "LEFT JOIN test_products as pro ON ord.prod_id = pro.prod_id where purorder.user_id =" + "'" + id.toString() + "' GROUP BY purorder.puord_id"
            try {
                global.TestDb.query(sql, function (err, rows, fields) {
                    if (err) {
                        console.log('err qeury:', err)
                        throw err
                    }
                    resolve({
                        status: true,
                        data: rows
                    })
                })
            } catch (e) {
                console.error('error find user from db :', e)
            }
        });
    }
}

module.exports = Order;
