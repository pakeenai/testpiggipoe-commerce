const _ = require('lodash');
var qs = require('qs');
const config = require('./config/' + process.env.NODE_ENV);
const storage = require('node-sessionstorage')
var mysql = require('mysql2');
const d = new Date();
var time = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds()

class User {

    async getUser(req) {
        const postData = req.query;
        let id = postData.id
        const sql = (id == "" || id == undefined) ? "SELECT * FROM users" : "SELECT * FROM users where user_id = " + "'" + id.toString() + "'"
        return new Promise((resolve, reject) => {
            try {
                global.TestDb.query(sql, function (err, rows, fields) {
                    if (err) {
                        console.log('err qeury:', err)
                        throw err
                    }
                    resolve({
                        status: true,
                        data: rows
                    })
                })
            } catch (e) {
                reject({
                    status: false,
                    message: 'Error db!'
                })
                console.error('error find user from db :', e)
            }
        });
    }
    async getHistory(req) {
        const postData = req.query;
        let id = postData.id
        if(id == "" || id == undefined){
            return {
                status: false,
                message: 'pls check id!'
            }
        }
        return new Promise((resolve, reject) => {
            const sql =  "SELECT purorder.puord_id,purorder.puord_status,purorder.puord_totalprice,JSON_ARRAYAGG(JSON_OBJECT('name',pro.prod_name ,'price',ord.ord_price,'status',ord.ord_status)) as products FROM test_purchaseorder as purorder LEFT JOIN test_order as ord ON ord.user_id = purorder.user_id "+
            "LEFT JOIN test_products as pro ON ord.prod_id = pro.prod_id where purorder.user_id ="  + "'" + id.toString() + "' GROUP BY purorder.puord_id"
            try {
                global.TestDb.query(sql, function (err, rows, fields) {
                    if (err) {
                        console.log('err qeury:', err)
                        throw err
                    }
                    resolve({
                        status: true,
                        data: rows
                    })
                })
            } catch (e) {
                console.error('error find user from db :', e)
            }
        });
    }
}

module.exports = User;
