const _ = require('lodash');
var qs = require('qs');
const config = require('./config/' + process.env.NODE_ENV);
const storage = require('node-sessionstorage')
const crypto = require('crypto');
const hash = crypto.createHash('sha256');
var mysql = require('mysql2');
const {sign} = require('jsonwebtoken');
const d = new Date();
const Test = require('./index');
var time =  d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()

function hasher(value) {
    const sha256Hasher = crypto.createHmac("sha256", 'shTestDev');
    const passhex = sha256Hasher.update(value).digest("hex");
    return passhex
}

class Auth {

    async getAuth(req) {
        const postData = req.body;
        let email = postData.email
        let pass = postData.password
        const hashpass = hasher(pass)
        return new Promise((resolve, reject) => {
                    if (email != undefined && pass != undefined) {
                        try {
                            var sql = "SELECT * FROM users where user_email = " + "'" + email.toString() + "'"
                            global.TestDb.query(sql, function (err, rows, fields) {
                                if (err) {
                                    console.log('err qeury:', err)
                                    throw err
                                }
                                if (rows.length > 0) {
                                    if (hashpass == rows[0].user_pass) {
                                        try {
                                            const user = {
                                                "id": rows[0].user_id,
                                                "email": postData.email,
                                            }
                                            const refreshToken = sign(user, config.SecretKey.refreshTokenSecret, { expiresIn: config.SecretKey.refreshTokenLife })
                                            const response = {
                                                "status": true,
                                                "access_token": refreshToken,
                                                "expires": 86000
                                            }
                                            console.log("response :", response)
                                            resolve(response)
                                        } catch (error) {
                                            resolve({
                                                status: false,
                                                err:error,
                                                message: 'Error jwt sign !'
                                            })
                                        }
                                    } else {
                                        resolve({
                                            status: false,
                                            message: 'Error To Password'
                                        })
                                    }
                                } else {
                                    resolve({
                                        status: false,
                                        message: 'Not Found User!'
                                    })
                                }
                            })
                        } catch (e) {
                            console.error('error find user from db :', e)
                        }

                    }
              
        });

    }

    async getRegister(req) {
        const postData = req.body;
        const user = {
            "fname": postData.fname,
            "lname": postData.lname,
            "email": postData.email,
            "pass": postData.password
        }
        return new Promise((resolve, reject) => {
        if (user.fname != '' && user.lname != '' && user.email != '' && user.pass != '') {
            try {
                var sql = "SELECT * FROM users where user_email = " + "'" + user.email.toString() + "'"
                global.TestDb.query(sql, function (err, rows, fields) {
                    if (err) {
                        console.log('err qeury:', rows)
                        throw err
                    }
                    if (rows.length > 0) {
                        resolve({
                            status: false,
                            message: 'Sorry, this email is already in the system.'
                        })
                    } else {
                        const hashpass = hasher(user.pass)
                        var sql = "INSERT INTO users (user_fname,user_lname,user_pass,user_email,create_date) VALUES ('" + user.fname + "','" + user.lname + "','"  + hashpass +  "','" + user.email.toString() + "','" + time + "')"
                        global.TestDb.query(sql, function (err, rows, fields) {
                            if (err) {
                                console.log('err qeury:', err)
                                throw err
                            }
                        })
                        resolve({
                            status: true,
                            message: 'Add User Success!'
                        })
                    }
                })

            } catch (err) {
                reject(err)
            }
        }
    });

    }
}

module.exports = Auth;
