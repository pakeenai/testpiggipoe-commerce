const cassandra = require('cassandra-driver');
var mysql = require('mysql2');
const config = require('./config/' + process.env.NODE_ENV);
const Auth = require('./Auth')
const axios = require('axios')
const storage = require('node-sessionstorage')
const express = require('express');
const app = express();
const cors = require('cors');
const LISTENPORT = process.env.LISTENPORT;

// const Redis = require("ioredis");


/**
 * @class Test
 */
class Test {

    constructor() {
        this.config = {
            ...this.config,
            ...config
        }
        // this.redis = new Redis(config.redis);
    }
    /**
     * Init Test Class
     * @returns {Promise<Test>}
     */
    async init() {
        console.log("init connect!!!!")
            this.TestDb = mysql.createConnection({
                host: this.config.DB.host,
                user: this.config.DB.username,
                password: this.config.DB.password,
                database: this.config.DB.keyspace,
                port:this.config.DB.port,
                connectionLimit: 10,
                acquireTimeout: 30000, //30 secs
            });
            await this.TestDb.connect();
            Object.defineProperty(this, this.config.DB.keyspace, {
                value: this.TestDb,
                writable: true,
                configurable: true
            });
        global.TestDb = this.TestDb
        return this;
    }

    /**
     * close all connections
     */
    shutdown() {
        global.Test_DB = false;
        this.TestDb.shutdown();
        super.shutdown();
    }
};

module.exports = Test;
