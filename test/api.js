// const mock = require('../mock/buzzebees');
const os = require('os');
const config = require('./config/' + os.userInfo().username);
process.env.NODE_ENV = config.NODE_ENV;
process.env.LISTENPORT = config.LISTENPORT;
const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
var qs = require('qs');
// Configure chai
chai.use(chaiHttp);
chai.should();

describe('test api', function (){
    this.timeout(120000);
    let server;
    
    before(async () => {
        server = await require('../api.js');
    });

    var body = {
        'email': 'admin@gmail.com',
        'password': 'admin',
    }

    it("should return HTTP Status 200", (done) => {
        console.log("config.url ::",config.url)
            .post('/v1/auth/login',body)
            .end((err, res) => {
                console.log("res ::",res.body)
                res.should.have.status(200);
                done();
            });
    });
})
