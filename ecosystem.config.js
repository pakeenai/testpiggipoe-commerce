module.exports = {
  apps : [{
    name      : 'TEst',
    script    : 'api.js',
    log_date_format: "YYYY-MM-DD HH:mm:ss",
    merge_logs: true,
    node_args: "--max-http-header-size=65535",
    env: {
      NODE_ENV: 'development',
      LISTENPORT: 9071
    }
  }],
};
