const express = require('express');
const app = express();
const cors = require('cors');
const path = require('path');
const YAML = require('yamljs');
const Loyalty = require('./sdk/index');
const Auth = require('./sdk/Auth');
const Users = require('./sdk/Users');
const Products = require('./sdk/Products');
const Order = require('./sdk/Order');
const LISTENPORT = process.env.LISTENPORT;
const config = require('./sdk/config/' + process.env.NODE_ENV);
global.Test = new Loyalty();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = YAML.load(__dirname  + '/api.yaml');
const bodyParser = require('body-parser')
const yaml = require('js-yaml');
const fs = require('fs');
const {sign,verify} = require('jsonwebtoken');

function Verify(req) {
    const accessToken = req.header('Authorization')?.split(" ")[1] || "";
    try {
        verify(accessToken, config.SecretKey.refreshTokenSecret);
        return true
     } catch(err) {
        console.log('err', err)
        return false
     }
}
module.exports = (async () => {
    const _auth = await new Auth();
    const users = await new Users();
    const products = await new Products();
    const order = await new Order();
    
    app.use(cors());
    app.use(express.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    await Test.init();
    try {
        try {
        // app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
        // // app.use(express.static(path.join(__dirname, './api')))
        // // // http://localhost:9014/api-docs/
        // app.use(bodyParser.text({ type: ['application/yaml', 'application/x-yaml', 'application/yml', 'application/x-yml', 'text/yaml', 'text/yml', 'text/x-yaml', 'text/x-yml'] }))
            const config = yaml.safeLoad(fs.readFileSync(__dirname  + '/api.yaml', 'utf8'));
            const indentedJson = JSON.stringify(config, null, 4);
            app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(config))
        } catch (e) {
            console.log(e);
        }
        app.post('/test/v1/auth/login', async (req, res)  => {
           var _res = await _auth.getAuth(req);
           return res.status(200).send({
                data:_res
           })
        })
        app.post('/test/v1/auth/register', async (req, res)  => {
            const verify = await Verify(req)
            if(!verify){
                return res.status(400).send({
                    status:false,
                    message: "Invalid token!"
               })
            }
            const _res = await _auth.getRegister(req);
            return res.status(200).send({
                 data:_res
            })            
         })
         app.get('/test/v1/users', async (req, res)  => {
            const verify = await Verify(req)
            if(!verify){
                return res.status(400).send({
                    status:false,
                    message: "Invalid token!"
               })
            }
            const _res = await users.getUser(req);
            return res.status(200).send({
                 data:_res
            })            
         })
         app.get('/test/v1/users/history', async (req, res)  => {
            const verify = await Verify(req)
            if(!verify){
                return res.status(400).send({
                    status:false,
                    message: "Invalid token!"
               })
            }
            const _res = await users.getHistory(req);
            return res.status(200).send({
                 data:_res
            })
         })
         app.get('/test/v1/products', async (req, res)  => {
            const verify = await Verify(req)
            if(!verify){
                return res.status(400).send({
                    status:false,
                    message: "Invalid token!"
               })
            }
            const _res = await products.getProducts(req);
            return res.status(200).send({
                 data:_res
            })            
         })
         
          app.post('/test/v1/order', async (req, res)  => {
            const verify = await Verify(req)
            if(!verify){
                return res.status(400).send({
                    status:false,
                    message: "Invalid token!"
               })
            }
            const _res = await order.AddOrder(req);
            return res.status(200).send({
                 data:_res
            })            
         })

         app.get('/test/v1/order/cancel', async (req, res)  => {
            const verify = await Verify(req)
            if(!verify){
                return res.status(400).send({
                    status:false,
                    message: "Invalid token!"
               })
            }
            const _res = await order.CancelOrder(req);
            return res.status(200).send({
                 data:_res
            })            
         })
         app.get('/test/v1/order', async (req, res)  => {
            const verify = await Verify(req)
            if(!verify){
                return res.status(400).send({
                    status:false,
                    message: "Invalid token!"
               })
            }
            const _res = await order.getOrder(req);
            return res.status(200).send({
                 data:_res
            })
         })
        
        app.listen(LISTENPORT, async () => {
            process.on('SIGINT', function () {
                TEst.shutdown();
                process.exit();
            });
            
           
            console.log("server start!")
        })

    } catch (e) {
        console.error(e);
        process.exit(1);
    }
})();
